package br.com.santander.bottelegram.java_hq_bot_telegram.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import br.com.santander.bottelegram.java_hq_bot_telegram.bot.SenderHelper;

@Component
public class SenderTaskComponent {

	private static final Logger LOGGER = LoggerFactory.getLogger(SenderTaskComponent.class);

	@Autowired
	private SenderHelper sender;
	
	public void run() {
		
		try {
			
			StringBuilder messageText = new StringBuilder("Alo BOT");
			
			String message = messageText.toString();
			
			sender.sendToAll(message);
			
			
		} catch (TelegramApiException e) {

			LOGGER.error("SENDER TASK COMPONENT", e);
			
		}
	}
	
	public SenderTaskComponent(SenderHelper sender) {
		this.sender = sender;
	}
	
}
