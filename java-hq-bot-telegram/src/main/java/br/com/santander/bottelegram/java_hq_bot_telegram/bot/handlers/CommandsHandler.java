package br.com.santander.bottelegram.java_hq_bot_telegram.bot.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import br.com.santander.bottelegram.java_hq_bot_telegram.bot.commands.StartCommand;
import br.com.santander.bottelegram.java_hq_bot_telegram.component.SenderTaskComponent;
import br.com.santander.bottelegram.java_hq_bot_telegram.configuration.BotConfiguration;
import br.com.santander.bottelegram.java_hq_bot_telegram.services.BusinessService;

public class CommandsHandler extends TelegramLongPollingCommandBot{

	private static final Logger LOGGER = LoggerFactory.getLogger(SenderTaskComponent.class);
	
	@Override
	public String getBotToken() {
		return BotConfiguration.ENV.get(BotConfiguration.BOT_TOKEN_KEY);
	}
	
	public CommandsHandler(DefaultBotOptions options, String botUsername, BusinessService hqService) {
		super(options, botUsername);
		
		register(new StartCommand());
		
		registerDefaultAction((absSender, message) -> {
			SendMessage commandUnknownMessage = new SendMessage();
			commandUnknownMessage.setChatId(message.getChatId());
			commandUnknownMessage.setText("Nao entendi sua solicitacao");
			
			try {
				
				absSender.execute(commandUnknownMessage);
				
			} catch (TelegramApiException e) {
				LOGGER.error("ERRO COMMANDS HANDLER", e);
			}
			
		});
	}
	
	@Override
	public void processNonCommandUpdate(Update update) {
		// TODO Auto-generated method stub
		
	}

}