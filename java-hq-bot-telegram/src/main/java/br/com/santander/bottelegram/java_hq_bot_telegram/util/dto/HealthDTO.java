package br.com.santander.bottelegram.java_hq_bot_telegram.util.dto;

public class HealthDTO {

	private String projectVersion;
	private String projectName;
	
	
	public HealthDTO(String projectVersion, String projectName) {
		super();
		this.projectVersion = projectVersion;
		this.projectName = projectName;
	}


	public String getProjectVersion() {
		return projectVersion;
	}


	public void setProjectVersion(String projectVersion) {
		this.projectVersion = projectVersion;
	}


	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
	
	
}
