package br.com.santander.bottelegram.java_hq_bot_telegram.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import br.com.santander.bottelegram.java_hq_bot_telegram.util.dto.HealthDTO;

@RestController
public class HealthEndpoint {

	private static final Logger LOGGER = LoggerFactory.getLogger(HealthEndpoint.class);
	
	private String projectVersion;
	
	private String projectName;
	
	public ResponseEntity<?> info(){
		LOGGER.debug(String.format("Health Check Application %s at %s", projectVersion, (new Date()).toString()));;
	
		return ResponseEntity.ok(new HealthDTO(projectVersion, projectName));
	}
}
