package br.com.santander.bottelegram.java_hq_bot_telegram.configuration;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.DefaultBotOptions.ProxyType;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import br.com.santander.bottelegram.java_hq_bot_telegram.component.SenderTaskComponent;
import br.com.santander.bottelegram.java_hq_bot_telegram.bot.handlers.CommandsHandler;
import br.com.santander.bottelegram.java_hq_bot_telegram.services.BusinessService;

@Configuration
public class BotConfiguration {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SenderTaskComponent.class);

	public static Map<String, String> ENV = new HashMap<String, String>();
	
	public static final String BOT_NAME_KEY = "BOT_NAME";
	
	public static final String BOT_TOKEN_KEY = "BOT_TOKEN";
	
	public static final String BOT_USERNAME_KEY = "BOT_USERNAME";
	
	public static final String BOT_CHATS_KEY = "BOT_CHATS";
	
	@Value("#{environment['BOT_CHATS']}")
	private String BOT_CHATS;
	
	@Value("#{environment['BOT_NAME']}")
	private String BOT_NAME;
	
	@Value("#{environment['BOT_TOKEN']}")
	private String BOT_TOKEN;
	
	@Value("#{environment['BOT_USERNAME']}")
	private String BOT_USERNAME;
	
	@Value("#{environment['PROXY_ENABLE']}")
	private boolean PROXY_ENABLE;
	
	@Value("#{environment['PROXY_HOST']}")
	private String PROXY_HOST;
	
	@Value("#{environment['PROXY_PORT']}")
	private int PROXY_PORT;
	
	@Autowired
	private BusinessService hqService;
	
	
	@PostConstruct
	public void init() {
		
		BotConfiguration.ENV.put(BotConfiguration.BOT_NAME_KEY, BOT_NAME);
		BotConfiguration.ENV.put(BotConfiguration.BOT_TOKEN_KEY, BOT_TOKEN);
		BotConfiguration.ENV.put(BotConfiguration.BOT_USERNAME_KEY, BOT_USERNAME);
		BotConfiguration.ENV.put(BotConfiguration.BOT_CHATS_KEY, BOT_CHATS);
		
		try {
		
			ApiContextInitializer.init();
			
			TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
			DefaultBotOptions options = new DefaultBotOptions();
			
			
			if(PROXY_ENABLE) {
				options.setProxyType(ProxyType.HTTP);
				options.setProxyHost(PROXY_HOST);
				options.setProxyPort(PROXY_PORT);
			}
			
			telegramBotsApi.registerBot(new CommandsHandler(options, BotConfiguration.ENV.get(BotConfiguration.BOT_TOKEN_KEY), hqService));
			
			
		} catch (TelegramApiException e) {
			LOGGER.error("BOT CONFIGURATION", e);
		}
		
	}
	
	
}

