package br.com.santander.bottelegram.java_hq_bot_telegram.bot;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import br.com.santander.bottelegram.java_hq_bot_telegram.configuration.BotConfiguration;

@Service
public class SenderHelper {

	private DefaultAbsSender sender;
	
	private String[] chatList;
	
	public SenderHelper(BotConfiguration botConfiguration) {
		super();
		
		chatList = BotConfiguration.ENV.get(BotConfiguration.BOT_CHATS_KEY).split(",");
		
		DefaultAbsSender _sender = new DefaultAbsSender(new DefaultBotOptions()) {
			
			@Override
			public String getBotToken() {
				return BotConfiguration.ENV.get(BotConfiguration.BOT_TOKEN_KEY);
			}
		};

		this.sender = _sender;
	}
	
	public DefaultAbsSender getSender() {
		return sender;
	}
	
	public boolean sendToAll(String message) throws TelegramApiException {
		
		for (int i = 0; i < chatList.length; i++) {
			
			String chatId = chatList[i];
			SendMessage echoMessage = new SendMessage();
			echoMessage.setChatId(chatId);
			echoMessage.setText(message.toString());
			getSender().execute(echoMessage);
		}

		return true;
		
	}
	
	
	public void execute(SendMessage message) throws TelegramApiException {
		
		getSender().execute(message);
	}
	
	public void setSender(DefaultAbsSender sender) {
		this.sender = sender;
	}
}
